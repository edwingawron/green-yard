---
title: Strona główna język angielski
hero-header-en: Your green world
hero-subtitle-en: We help create
hero-text-en: We appear on the market and we build gardens
portfolio-header-en: Portfolio
portfolio-subtitle-en: Portfolio subtitle
services-header-en: Our services
services-text-en: 'Permanent, experienced staff and meeting European machinery park standards.'
about-us-header-en: Our history
about-us-subtitle-en: Read
about-us-text-en: >-
  See for yourself why administrators of cities such as Poznan, Wroclaw and
  Zakopane have trusted us for whom for many years we have been providing
  services in the area of ​​greenery maintenance and cleaning works.
our-clients-header-en: Our clients
our-clients-subtitle-en: Project's name
our-clients-text-en: >-
  Pictures, abstract symbols, materials, and colors are among the ingredients
  with which a designer or engineer works. To design is to discover
  relationships and to make arrangements and rearrangements among these
  ingredients. Pictures, abstract symbols, materials, and colors are among the
  ingredients with which a designer or engineer works. To design is to discover
  relationships and to make arrangements and rearrangements among these
  ingredients.
contact-header-en: Let's talk
contact-text-en: >-
  Versatile, rich and diverse offer, a permanent team of excellent specialists,
  professional and innovative equipment base, own transport base, over a dozen
  years of experience, implementation of the most prestigious investments in
  Poland and presence on international markets make Park-M a trusted partner of
  the most demanding customers.
---


---
title: Dane strony
site-owner: Michał Kapusta
site-company: Green Yard
owner-email: info@greenyard.pl
owner-phone: '601959740'
owner-phone-displayed: 601 959 740
streetAddress: 'ul. Waszyngtona 24/21 '
postalCode: 42-217
addressLocality: Częstochowa
addressRegion: woj. śląskie 
map: mapa
short-description: >-
  Urządzanie terenów zielonych w otoczeniu obiektów przemysłowych, handlowych i prywatnych.
meta-description:
  en: Meta description
  pl: >-
    Green Yard świadczy kompleksowe usługi z zakresu urządzania terenów zielonych. Realizujemy projekty ogrodów przydomowych, terenów zieleni w otoczeniu obiektów przemysłowych i handlowych. Oferta Green Yard obejmuje również usługi z zakresu pielęgnacji terenów zieleni, montażu i serwisu automatycznych systemów nawadniających oraz fachowe doradztwo.
social-media:
  facebook: 'https://www.facebook.com/'
  instagram: 'https://www.instagram.com/ogrody.greenyard/'
---


---
title: 01 Usługa test
layout: clients
lang: pl
name: Pielęgnacja i serwis ogrodów
image: /images/services/ogrod_w_czestochowie-sm.png
blurb: |-
  Projekty ogrodów przydomowych <br />
  Renowacja i ochrona trawników <br />
  Aranżacja zieleni <br />
  Systemy nawadniające <br />
  Pielęgnacja roślinności ogrodowej <br />
  Rewaloryzacja ogrodów <br />
---
Blokowe lub **kamienne** ściany

Przywrócenie linii brzegowej

Usuwanie gatunków inwazyjnych

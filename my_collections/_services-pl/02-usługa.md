---
title: 02 Usługa
layout: clients
lang: pl
name: Zieleń obiektów przemysłowych i&nbsphandlowych
image: /images/services/zielen-obiektow-przemyslowych-sm.png
blurb: |-
  Kompleksowa pielęgnacja zieleni <br />
  Projekty terenów zieleni publicznej <br />
  Adaptacja zieleni otoczenia budynków <br />
  Skwery, dziedzince, place miejskie <br />
  Inwentaryzacja zieleni <br />
---
Blokowe lub **kamienne** ściany

Przywrócenie linii brzegowej

Usuwanie gatunków inwazyjnych

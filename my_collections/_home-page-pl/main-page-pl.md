---
title: Strona główna język polski
hero-header-pl: Twój zielony świat
hero-subtitle-pl: Zobacz jak możemy odmienić Twoje miejsce na ziemi.
hero-text-pl: >-
  "Najlepszy czas, aby zasadzić drzewo, był 20 lat temu. Drugi najlepszy czas
  jest teraz." <br /> <i>- chińskie przysłowie</i>
portfolio-header-pl: Realizacje
portfolio-subtitle-pl: Realizacje podtytuł
services-header-pl: Nasze usługi
services-text-pl: >-
  Oferujemy szeroki zakres usług ogrodniczych. Dysponujemy wiedzą,
  doświadczeniem i&nbspsolidnym sprzętem, a nasz największy atut to stała,
  zgrana kadra.
about-us-header-pl: Green Yard
about-us-subtitle-pl: Poznaj
about-us-text-pl: >-
  W Green Yard zawsze koncentrowaliśmy się na jednej rzeczy: zadowoleniu
  klienta. Zapewniamy tę satysfakcję dzięki naszej kompleksowej ofercie
  specjalistycznych usług w zakresie pielęgnacji trawników i kształtowania
  krajobrazu, w tym pielęgnacji terenów mieszkalnych i komercyjnych.
our-clients-header-pl: Konopiska
our-clients-subtitle-pl: Zaufali nam
our-clients-text-pl: >-
  Wiele zaufanych relacji, które zbudowaliśmy, było możliwe tylko dzięki
  konsekwentnemu budowaniu wizji życia na świeżym powietrzu. Obszarem działania
  Green Yard jest Śląsk. Świadczymy usługi ogrodnicze w Częstochowie, a także w
  miastach jak Katowice, Bytom, Chorzów, Sosnowiec itd...
contact-header-pl: Porozmawiajmy
contact-text-pl: >-
  Skontaktuj się z nami, aby uzyskać bezpłatną wycenę. Zapraszamy klientów
  indywidualnych, biznesowych i instytucjonalnych z Częstochowy oraz innych
  miast województwa śląskiego. Architektura krajobrazu i ogrodu to nasza
  specjalność!
gallery:
  - alt-text: Green Yard Iglaki zimą
    gallery-image: /images/homepage-gallery/green-yard-iglaki-zima.png
    gallery-image-thumb: /images/homepage-gallery/green-yard-iglaki-zima-sm.png
  - alt-text: Green Yard kwiaty zimą
    gallery-image: /images/homepage-gallery/green-yard-kwiaty-zima.png
    gallery-image-thumb: /images/homepage-gallery/green-yard-kwiaty-zima-sm.png
  - alt-text: Green Yard trawnik zimą
    gallery-image: /images/homepage-gallery/green-yard-trawnik-zima.png
    gallery-image-thumb: /images/homepage-gallery/green-yard-trawnik-zima-sm.png
  - alt-text: Green Yard zieleń przed domem
    gallery-image: /images/homepage-gallery/green-yard-zielen-przed-domem.png
    gallery-image-thumb: /images/homepage-gallery/green-yard-zielen-przed-domem-sm.png
  - alt-text: Green Yard zieleń przed domem
    gallery-image: /images/homepage-gallery/green-yard-zielen-przed-domem-donice.png
    gallery-image-thumb: /images/homepage-gallery/green-yard-zielen-przed-domem-donice-sm.png
  - alt-text: Green Yard zieleń w biurze
    gallery-image: /images/homepage-gallery/green-yard-zielen-w-biurze.png
    gallery-image-thumb: /images/homepage-gallery/green-yard-zielen-w-biurze-sm.png
  - alt-text: Green Yard zieleń w biurze
    gallery-image: /images/homepage-gallery/green-yard-zielen-w-biurze-donice.png
    gallery-image-thumb: /images/homepage-gallery/green-yard-zielen-w-biurze-donice-sm.png
  - alt-text: Green Yard zieleń zimą
    gallery-image: /images/homepage-gallery/green-yard-zielen-zima.png
    gallery-image-thumb: /images/homepage-gallery/green-yard-zielen-zima-sm.png
---


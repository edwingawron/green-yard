---
title: 5
layout: photoswipe
lang: pl
portfolio-col-pl-name: Ogród kameralny
portfolio-col-pl-name-alt: Ogrody przydomowe
cover-image: /images/ogrod-kameralny/ogrod_kameralny-06.png
cover-sm: /images/ogrod-kameralny/ogrod_kameralny-sm.png
portfolio-col-pl-desc: >-
  Wysokie żywopłoty grabowe zapewniają domownikom prywatność. Duża ilość traw ozdobnych przełamuje surowe, geometryczne kształty ogrodu. Mur obłożony naturalnym łupkiem oddziela dwa piętra ogrodu tworząc płaską strefę przeznaczoną do gry w piłkę nożną.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-01.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-02.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-03.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-04.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-05.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-06.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-07.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-08.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-09.png
  - portfolio-col-pl-image: /images/ogrod-kameralny/ogrod_kameralny-10.png
---


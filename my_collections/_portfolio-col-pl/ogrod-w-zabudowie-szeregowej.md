---
title: 6
layout: photoswipe
lang: pl
portfolio-col-pl-name: Ogród w zabudowie szeregowej
portfolio-col-pl-name-alt: Ogrody przydomowe
cover-image: /images/ogrod-szeregowy/ogrod_w_zabudowie_szeregowej-02.png
cover-sm: /images/ogrod-szeregowy/ogrod_w_zabudowie_szeregowej-sm.png
portfolio-col-pl-desc: >-
  Maleńka działka przy szeregówce to prawdziwe wyzwanie. Projektantka stworzyła przytulny ogród będący kontynuacją domowego salonu. Rezygnacja z trawnika i wykorzystanie kamienia do ściółkowania minimalizuje czas i koszt obsługi ogrodu. Jasne kolory użytych materiałów oraz lustro na ścianie optycznie powiększają przestrzeń.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/ogrod-szeregowy/ogrod_w_zabudowie_szeregowej-01.png
  - portfolio-col-pl-image: /images/ogrod-szeregowy/ogrod_w_zabudowie_szeregowej-02.png
  - portfolio-col-pl-image: /images/ogrod-szeregowy/ogrod_w_zabudowie_szeregowej-03.png
  - portfolio-col-pl-image: /images/ogrod-szeregowy/ogrod_w_zabudowie_szeregowej-04.png
  - portfolio-col-pl-image: /images/ogrod-szeregowy/ogrod_w_zabudowie_szeregowej-05.png
---


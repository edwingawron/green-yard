---
title: 3
layout: photoswipe
lang: pl
portfolio-col-pl-name: Ogród w Częstochowie
portfolio-col-pl-name-alt: Ogrody przydomowe
cover-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-06.png
cover-sm: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-sm.png
portfolio-col-pl-desc: >-
  Green Yard pomógł mi przekształcić mój przestarzały, patchworkowy dziedziniec
  w zachęcający Ogród! Architekt krajobrazu współpracował ze mną, aby
  zrealizować moją koncepcję od projektu do rzeczywistości - zapewniając cenne
  wskazówki na temat tego, co się sprawdzi, a co może być złym pomysłem.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-01.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-02.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-03.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-04.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-05.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-06.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-07.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-08.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-09.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-10.png
  - portfolio-col-pl-image: /images/ogrod-w-czestochowie/ogrod_w_czestochowie-11.png
---


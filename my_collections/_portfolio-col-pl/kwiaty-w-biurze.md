---
title: 8
layout: photoswipe
lang: pl
portfolio-col-pl-name: Kwiaty w biurze
portfolio-col-pl-name-alt: Zieleń do biura
cover-image: /images/kwiaty-biurowe/kwiaty_w_biurze-08.png
cover-sm: /images/kwiaty-biurowe/kwiaty_w_biurze-sm.png
portfolio-col-pl-desc: >-
  Dobra atmosfera pracy w biurze to nie tylko ergonomiczne krzesło i właściwe oświetlenie. Odpowiednio dobrane rośliny to atrakcyjny element designu zwiększający estetykę wnętrza ale to nie wszystko. Kwiaty w biurze dają realne korzyści  dla zdrowia i efektywności pracy.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-01.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-02.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-03.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-04.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-05.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-06.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-07.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-08.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-09.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-10.png
  - portfolio-col-pl-image: /images/kwiaty-biurowe/kwiaty_w_biurze-11.png
---
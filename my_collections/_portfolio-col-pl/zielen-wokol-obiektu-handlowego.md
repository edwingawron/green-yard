---
title: 9
layout: photoswipe
lang: pl
portfolio-col-pl-name: Zieleń wokół obiektu handlowego
portfolio-col-pl-name-alt: Zieleń towarzysząca obiektom budowlanym
cover-image: /images/services/zielen-obiektow-przemyslowych.jpg
cover-sm: /images/services/zielen-obiektow-przemyslowych-sm.jpg
portfolio-col-pl-desc: >-
  Green Yard pomógł mi przekształcić mój przestarzały, patchworkowy dziedziniec w zachęcający Ogród! Architekt krajobrazu współpracował ze mną, aby zrealizować moją koncepcję od projektu do rzeczywistości - zapewniając cenne wskazówki na temat tego, co się sprawdzi, a co może być złym pomysłem.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/zielen-obiekt-handlowy/zielen_wokol_obiektu_handlowego-01.png
  - portfolio-col-pl-image: /images/zielen-obiekt-handlowy/zielen_wokol_obiektu_handlowego-02.png
  - portfolio-col-pl-image: /images/zielen-obiekt-handlowy/zielen_wokol_obiektu_handlowego-03.png
  - portfolio-col-pl-image: /images/zielen-obiekt-handlowy/zielen_wokol_obiektu_handlowego-05.png
---
---
title: 1
layout: photoswipe
lang: pl
portfolio-col-pl-name: Nowoczesny ogród
portfolio-col-pl-name-alt: Ogrody przydomowe
cover-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-11.png
cover-sm: /images/nowoczesny-ogrod/nowoczesny_ogrod-sm.png
portfolio-col-pl-desc: >-
  Nowoczesny dom w mieście wymaga nowoczesnego otoczenia. Ogród nietuzinkowy, a jednocześnie bardzo prosty w swojej formie. Geometryczne kształty dopełniają bryłę budynku, mała ilość zastosowanych gatunków pozwala na zachowanie spokoju kompozycji i ułatwia utrzymanie wysokiego standardu pielęgnacji. Sezonowe nasadzenia w donicach dodają świeżości i koloru.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-01.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-02.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-03.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-04.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-05.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-06.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-07.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-08.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-09.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-10.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-11.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-12.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-13.png
  - portfolio-col-pl-image: /images/nowoczesny-ogrod/nowoczesny_ogrod-14.png
---


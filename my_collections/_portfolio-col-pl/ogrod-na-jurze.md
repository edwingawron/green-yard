---
title: 2
layout: photoswipe
lang: pl
portfolio-col-pl-name: Ogród na Jurze
portfolio-col-pl-name-alt: Ogrody przydomowe
cover-image: /images/ogrod-na-jurze/ogrod_na_jurze-11.png
cover-sm: /images/ogrod-na-jurze/ogrod_na_jurze-sm.png
portfolio-col-pl-desc: >-
  Duża, ciekawie ukształtowana działka i nowoczesny dom. Zależało nam na stworzeniu otoczenia, które będzie nawiązywało do malowniczych terenów Jury. Buki, sosny, brzozy i trawy to zapożyczenie bezpośrednio z okolicznych krajobrazów. Duże rabaty bylinowe łagodnie przechodzą w grupy zieleni wysokiej. Lawendy, hortensje i róże ożywiają ogród w drugiej połowie lata. Wszystko spięte w łagodnych naturalnych kształtach tak by jak najbardziej zlewać się z okoliczną zielenią.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-01.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-02.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-03.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-04.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-05.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-06.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-07.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-08.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-09.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-10.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-11.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-12.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-13.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-14.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-15.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-16.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-17.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-18.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-19.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-20.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-21.png
  - portfolio-col-pl-image: /images/ogrod-na-jurze/ogrod_na_jurze-22.png
---
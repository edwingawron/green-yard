---
title: 4
layout: photoswipe
lang: pl
portfolio-col-pl-name: Ogród rezydencyjny
portfolio-col-pl-name-alt: Ogrody przydomowe
cover-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-04.png
cover-sm: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-sm.png
portfolio-col-pl-desc: >-
  Zadanie jakie zlecono nam na prezentowanej inwestycji ograniczało się do konsultacji projektowanych roślin, dostarczenia większości z nich i wykonania nasadzeń. W ciągu kilku miesięcy prac posadziliśmy ponad 50 000 sztuk krzewów, bylin i drzew. Praca przy tym projekcie to dla nas zaszczyt i bezcenne doświadczenie, a efekty prac oglądane po kilku latach to największa nagroda za poświęcony czas i wysiłek.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-01.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-02.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-03.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-04.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-05.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-06.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-07.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-08.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-09.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-10.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-11.png
  - portfolio-col-pl-image: /images/ogrod-rezydencyjny/ogrod_rezydencyjny-12.png
---
---
title: 7
layout: photoswipe
lang: pl
portfolio-col-pl-name: Rośliny oczyszczające powietrze
portfolio-col-pl-name-alt: Zieleń do biura
cover-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-09.png
cover-sm: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-sm.png
portfolio-col-pl-desc: >-
  Zieleń w biurze to naturalnie oczyszczone powietrze, lepsza koncentracja i samopoczucie pracowników. Badania naukowe dowodzą, że rośliny doniczkowe redukują stres, a poprawa jakości powietrza wpływa na usprawnienie procesów decyzyjnych.
portfolio-col-pl-gallery:
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-01.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-02.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-03.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-04.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-05.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-06.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-07.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-08.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-09.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-10.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-11.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-12.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-13.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-14.png
  - portfolio-col-pl-image: /images/rosliny-oczyszczajace-powietrze/rosliny_oczyszczajace_powietrze-15.png
---
// Netlify Identity
if (window.netlifyIdentity) {
  window.netlifyIdentity.on("init", user => {
    if (!user) {
      window.netlifyIdentity.on("login", () => {
        document.location.href = "/admin/";
        });
      }
    });
  };

// Menu
function toggleMenu() {
  var btn = document.getElementById("menu-button");
  var element = document.getElementById("menu-full");
  btn.classList.toggle("open");

  if (element.className == "is-hidden") element.className = "full-menu";
  else element.className = "is-hidden";
}


// Scroll
function scrollToTop() {
  window.scroll({ top: 0, left: 0, behavior: "smooth" }); 
}

function scrollDown() {
  window.scrollBy({ top: 800, left: 0, behavior: "smooth" });
}


// Cookies
(function() {
  //copyright JGA 2013 under MIT License
  var monster = {
    set: function(e, t, n, r) {
      var i = new Date(),
        s = '',
        o = typeof t,
        u = '';
      (r = r || '/'),
        n &&
          (i.setTime(i.getTime() + n * 24 * 60 * 60 * 1e3),
          (s = '; expires=' + i.toGMTString()));
      if (o === 'object' && o !== 'undefined') {
        if (!('JSON' in window))
          throw "Bummer, your browser doesn't support JSON parsing.";
        u = JSON.stringify({ v: t });
      } else u = escape(t);
      document.cookie = e + '=' + u + s + '; path=' + r;
    },
    get: function(e) {
      var t = e + '=',
        n = document.cookie.split(';'),
        r = '',
        i = '',
        s = {};
      for (var o = 0; o < n.length; o++) {
        var u = n[o];
        while (u.charAt(0) == ' ') u = u.substring(1, u.length);
        if (u.indexOf(t) === 0) {
          (r = u.substring(t.length, u.length)), (i = r.substring(0, 1));
          if (i == '{') {
            s = JSON.parse(r);
            if ('v' in s) return s.v;
          }
          return r == 'undefined' ? undefined : unescape(r);
        }
      }
      return null;
    },
    remove: function(e) {
      this.set(e, '', -1);
    },
    increment: function(e, t) {
      var n = this.get(e) || 0;
      this.set(e, parseInt(n, 10) + 1, t);
    },
    decrement: function(e, t) {
      var n = this.get(e) || 0;
      this.set(e, parseInt(n, 10) - 1, t);
    }
  };

  if (monster.get('cookieinfo') === 'true') {
    return false;
  }

  var container = document.createElement('div'),
    link = document.createElement('a');

  container.setAttribute('id', 'cookieinfo');
  container.setAttribute('class', 'cookie-alert');
  container.innerHTML =
    "<h6>Ta strona wykorzystuje pliki cookie</h6><p>Na naszej witrynie stosujemy pliki cookies w celu świadczenia Państwu usług na najwyższym poziomie w sposób dostosowany do indywidualnych potrzeb. Korzystanie z witryny bez zmiany ustawień dotyczących cookies oznacza, że będą one zamieszczone w Państwa urządzeniu końcowym. Możecie Państwo dokonać w każdym czasie zmiany ustawień dotyczących cookies w swojej przeglądarce.</p>";

  link.setAttribute('href', '#');
  link.setAttribute('title', 'Zamknij');
  link.innerHTML = 'x';

  function clickHandler(e) {
    if (e.preventDefault) {
      e.preventDefault();
    } else {
      e.returnValue = false;
    }

    container.setAttribute('style', 'opacity: 1');

    var interval = window.setInterval(function() {
      container.style.opacity -= 0.01;

      if (container.style.opacity <= 0.02) {
        document.body.removeChild(container);
        window.clearInterval(interval);
        monster.set('cookieinfo', 'true', 365);
      }
    }, 4);
  }

  if (link.addEventListener) {
    link.addEventListener('click', clickHandler);
  } else {
    link.attachEvent('onclick', clickHandler);
  }

  container.appendChild(link);
  document.body.appendChild(container);

  return true;
})();



// Photoswipe
var initPhotoSwipeFromDOM = function(gallerySelector) {
  var parseThumbnailElements = function(el) {
    var thumbElements = el.childNodes,
      numNodes = thumbElements.length,
      items = [],
      figureEl,
      linkEl,
      size,
      item;

    for (var i = 0; i < numNodes; i++) {
      figureEl = thumbElements[i];

      if (figureEl.nodeType !== 1) {
        continue;
      }

      linkEl = figureEl.children[0];

      size = linkEl.getAttribute("data-size").split("x");

      item = {
        src: linkEl.getAttribute("href"),
        w: parseInt(size[0], 10),
        h: parseInt(size[1], 10)
      };

      if (figureEl.children.length > 1) {
        item.title = figureEl.children[1].innerHTML;
      }

      if (linkEl.children.length > 0) {
        item.msrc = linkEl.children[0].getAttribute("src");
      }

      item.el = figureEl;
      items.push(item);
    }
    return items;
  };

  var closest = function closest(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
  };

  var onThumbnailsClick = function(e) {
    e = e || window.event;
    e.preventDefault ? e.preventDefault() : (e.returnValue = false);

    var eTarget = e.target || e.srcElement;

    var clickedListItem = closest(eTarget, function(el) {
      return el.tagName && el.tagName.toUpperCase() === "FIGURE";
    });

    if (!clickedListItem) {
      return;
    }

    var clickedGallery = clickedListItem.parentNode,
      childNodes = clickedListItem.parentNode.childNodes,
      numChildNodes = childNodes.length,
      nodeIndex = 0,
      index;

    for (var i = 0; i < numChildNodes; i++) {
      if (childNodes[i].nodeType !== 1) {
        continue;
      }

      if (childNodes[i] === clickedListItem) {
        index = nodeIndex;
        break;
      }
      nodeIndex++;
    }

    if (index >= 0) {
      openPhotoSwipe(index, clickedGallery);
    }
    return false;
  };

  var photoswipeParseHash = function() {
    var hash = window.location.hash.substring(1),
      params = {};

    if (hash.length < 5) {
      return params;
    }

    var vars = hash.split("&");
    for (var i = 0; i < vars.length; i++) {
      if (!vars[i]) {
        continue;
      }
      var pair = vars[i].split("=");
      if (pair.length < 2) {
        continue;
      }
      params[pair[0]] = pair[1];
    }

    if (params.gid) {
      params.gid = parseInt(params.gid, 10);
    }
    return params;
  };

  var openPhotoSwipe = function(
    index,
    galleryElement,
    disableAnimation,
    fromURL
  ) {
    var pswpElement = document.querySelectorAll(".pswp")[0],
      gallery,
      options,
      items;

    items = parseThumbnailElements(galleryElement);

    options = {
      galleryUID: galleryElement.getAttribute("data-pswp-uid"),

      getThumbBoundsFn: function(index) {
        var thumbnail = items[index].el.getElementsByTagName("img")[0],
          pageYScroll =
            window.pageYOffset || document.documentElement.scrollTop,
          rect = thumbnail.getBoundingClientRect();
        return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
      }
    };

    if (fromURL) {
      if (options.galleryPIDs) {
        for (var j = 0; j < items.length; j++) {
          if (items[j].pid == index) {
            options.index = j;
            break;
          }
        }
      } else {
        options.index = parseInt(index, 10) - 1;
      }
    } else {
      options.index = parseInt(index, 10);
    }
    if (isNaN(options.index)) {
      return;
    }
    if (disableAnimation) {
      options.showAnimationDuration = 0;
    }
    gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
  };

  var galleryElements = document.querySelectorAll(gallerySelector);

  for (var i = 0, l = galleryElements.length; i < l; i++) {
    galleryElements[i].setAttribute("data-pswp-uid", i + 1);
    galleryElements[i].onclick = onThumbnailsClick;
  }

  var hashData = photoswipeParseHash();
  if (hashData.pid && hashData.gid) {
    openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
  }
};
initPhotoSwipeFromDOM(".my-gallery");